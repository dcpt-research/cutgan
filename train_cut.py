import time
import torch
from options.train_options import TrainOptions
from data import create_dataset
from models import *#create_model
from util.visualizer import Visualizer
from dataset import CTCBCTDataset
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
import itertools
#from models.cyclecut_model import CUTModel
import os
from torch.utils.data.distributed import DistributedSampler
import torch.multiprocessing as mp
from torch.nn.parallel import DistributedDataParallel as DDP
import torch.distributed as dist
from tqdm import tqdm

os.environ[
        "TORCH_DISTRIBUTED_DEBUG"
    ] = "DETAIL"

writer= SummaryWriter("runs/CycleGAN")

layout = {
    "Synthetic CT generation from CBCT": {
        "loss training": ["Multiline", ["lossT/generator_H", "lossT/discriminator_H","lossT/generator_Z", "lossT/discriminator_Z"]],
        "loss validation": ["Multiline", ["lossV/generator_H", "lossV/discriminator_H","lossV/generator_Z", "lossV/discriminator_Z"]],
    },
}
writer.add_custom_scalars(layout)

def setup(rank, world_size):
    os.environ['MASTER_ADDR'] = 'localhost'
    os.environ['MASTER_PORT'] = '12355'
    dist.init_process_group("nccl", rank=rank, world_size=world_size)

def cleanup():
    dist.destroy_process_group()

def prepare(rank, world_size, root, train=True, batch_size=2, pin_memory=False, num_workers=12,output_size=164, output_depth=32):
    dataset = CTCBCTDataset(root_CT=root+"/CT", root_CBCT=root+"/CBCT", train=train,output_size=output_size, output_depth=output_depth)
    sampler = DistributedSampler(dataset, num_replicas=world_size, rank=rank, shuffle=False, drop_last=False)
    dataloader = DataLoader(dataset, batch_size=batch_size, pin_memory=pin_memory, num_workers=num_workers, drop_last=False, shuffle=False, sampler=sampler)
    return dataloader

def main(rank, world_size):
    setup(rank, world_size)
    opt = TrainOptions().parse()
    model = create_model(opt,rank,world_size)
    
    # prepare the dataloader
    train_root=os.path.join(opt.dataroot,"train")
    validation_root=os.path.join(opt.dataroot,"validation")

    dataloader = prepare(rank, world_size, train_root, train=True, batch_size=opt.batch_size, num_workers=opt.num_threads,output_size=opt.output_size, output_depth=opt.output_depth)
    dataloader_val = prepare(rank, world_size, validation_root, train=False, batch_size=opt.batch_size, num_workers=opt.num_threads,output_size=opt.output_size, output_depth=opt.output_depth)
    
    total_iters = 0
    total_iters_val = 0 
    epoch_iter = 0
    for epoch in range(opt.epoch_count, opt.n_epochs + opt.n_epochs_decay + 1):
        
        epoch_start_time = time.time()
        
        dataloader.sampler.set_epoch(epoch)
        dataloader_val.sampler.set_epoch(epoch)
        
        loop = tqdm(dataloader, leave=False) #gives a nice progress bar
        loop.set_description("Training loop: Epoch %s" % epoch)
        
        for idx, data in enumerate(loop):
            batch_size = data[0].size(0)
            total_iters += batch_size
            epoch_iter += batch_size
            
            if idx==0 and epoch == opt.epoch_count:
                model.data_dependent_initialize(data,train=True)
                model.setup(opt)               # regular setup: load and print networks; create schedulers
            
            model.set_input(data)    
            model.optimize_parameters() #den her er den der tager lang tid
    
            if idx % opt.display_freq == 0: #tensorboard for losses
                model.plot_losses_to_tensorboard(writer,"train",total_iters)
                
                
            if idx % opt.display_freq_img == 0: #tensorboard for images
                model.plot_images_to_tensorboard(writer,"train",total_iters)

        
        val_loop = tqdm(dataloader_val, leave=False) #gives a nice progress bar
        val_loop.set_description("Validation loop: Epoch %s" % epoch)

        for idx_val, data in enumerate(val_loop):
            batch_size = data[0].size(0)
            total_iters_val += batch_size
            
            if idx_val==0 and epoch == opt.epoch_count:
                model.data_dependent_initialize(data,train=False)
            
            model.set_input(data)
            model.test() #den her er den der tager lang tid
            
            if idx_val % opt.display_freq_val == 0: #tensorboard for losses
                model.plot_losses_to_tensorboard(writer,"validation",total_iters_val)
                
            if idx_val % opt.display_freq_img_val == 0: #tensorboard for images
                model.plot_images_to_tensorboard(writer,"validation",total_iters_val)
                
        if epoch % opt.save_epoch_freq == 0:              # cache our model every <save_epoch_freq> epoch
            model.save_networks('latest',rank)
            model.save_networks(epoch,rank)
            dist.barrier()
            if rank==0:
                print('saving the model at the end of epoch %d, iters %d' % (epoch, total_iters))
        
        if rank==0:
            print('End of epoch %d / %d \t Time Taken: %d sec' % (epoch, opt.n_epochs + opt.n_epochs_decay, time.time() - epoch_start_time))
        
        model.update_learning_rate()                     # update learning rates at the end of every epoch.
    
    cleanup()


if __name__ == '__main__':
    # suppose we have 3 gpus
    world_size = 2
    mp.spawn(
        main,
        args=(world_size,),
        nprocs=world_size
    )
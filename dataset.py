import os
from torch.utils.data import Dataset
import numpy as np
import monai


class CTCBCTDataset(Dataset): 
    def __init__(self, root_CBCT, root_CT, train=True,output_size=196, output_depth=32):
        self.root_CBCT = root_CBCT #set directory for CBCT images
        self.root_CT = root_CT #set directory for CBCT images
        self.train=train #set transforms (standard = None)
        self.size=output_size
        self.depth=output_depth
    

        self.CBCT_images = os.listdir(root_CBCT) #CBCT images
        self.CT_images = os.listdir(root_CT) #CT images
        self.length_dataset = max(len(self.CBCT_images), len(self.CT_images)) #max number of images
        self.CBCT_len = len(self.CBCT_images)
        self.CT_len = len(self.CT_images)

    def __len__(self):
        return self.length_dataset

    def __getitem__(self, index):

        np.random.shuffle(self.CBCT_images)
        np.random.shuffle(self.CT_images)
        
        CBCT_img = self.CBCT_images[index % self.CBCT_len] #ensures that indexing is in the correct range
        CT_img = self.CT_images[index % self.CT_len] #ensures that indexing is in the correct range
        
        CBCT_path = os.path.join(self.root_CBCT, CBCT_img) #total path of CBCT image
        CT_path = os.path.join(self.root_CT, CT_img) #total path of CT image

        #CBCT
        CBCT_npz_file=np.load(CBCT_path)
        CBCT_image = CBCT_npz_file['arr_0']
        CBCT_image_rot=CBCT_image[:, ::-1, :]
        CT_npz_file=np.load(CT_path)
        CT_image = CT_npz_file['arr_0']
        CT_image_rot=CT_image[:, ::-1, :] 
        CT_dic={}
        CT_dic['CBCT']=np.expand_dims(CBCT_image_rot,0)
        CT_dic['CT']=np.expand_dims(CT_image_rot,0)


        ## get indices for specific patch:
        #print(CT_dic['CBCT'].shape)

        ## agumentations
        
        Agumentation_monai=monai.transforms.Compose([
            monai.transforms.RandSpatialCropd(keys=['CBCT', 'CT'], roi_size=(self.depth,self.size,self.size), random_size=False),
            monai.transforms.RandAxisFlipd(keys=['CBCT', 'CT'], prob=0.5),
            monai.transforms.GibbsNoised(keys='CBCT', alpha=0.2),
            monai.transforms.RandScaleIntensityd(keys='CBCT',factors=0.01, prob=0.1),
            monai.transforms.ToTensor()
        ])

        Agumentation_monai_val=monai.transforms.Compose([
            monai.transforms.RandSpatialCropd(keys=['CBCT', 'CT'], roi_size=(self.depth,self.size,self.size), random_size=False),
            monai.transforms.ToTensor(),
        ])

        if self.train==True:
            CT_image_aug=Agumentation_monai(CT_dic)
        else:
            CT_image_aug=Agumentation_monai_val(CT_dic)
        return CT_image_aug["CBCT"], CT_image_aug["CT"]

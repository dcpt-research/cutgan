import torch
import itertools
from util.image_pool import ImagePool
from .base_model import BaseModel
from . import networks
import torchvision
import random
import numpy as np

class CycleGANModel(BaseModel):
    """
    This class implements the CycleGAN model, for learning image-to-image translation without paired data.

    The model training requires '--dataset_mode unaligned' dataset.
    By default, it uses a '--netG resnet_9blocks' ResNet generator,
    a '--netD basic' discriminator (PatchGAN introduced by pix2pix),
    and a least-square GANs objective ('--gan_mode lsgan').

    CycleGAN paper: https://arxiv.org/pdf/1703.10593.pdf
    """
    @staticmethod
    def modify_commandline_options(parser, is_train=True):
        """Add new dataset-specific options, and rewrite default values for existing options.

        Parameters:
            parser          -- original option parser
            is_train (bool) -- whether training phase or test phase. You can use this flag to add training-specific or test-specific options.

        Returns:
            the modified parser.

        For CycleGAN, in addition to GAN losses, we introduce lambda_A, lambda_B, and lambda_identity for the following losses.
        A (source domain), B (target domain).
        Generators: G_A: A -> B; G_B: B -> A.
        Discriminators: D_A: G_A(A) vs. B; D_B: G_B(B) vs. A.
        Forward cycle loss:  lambda_A * ||G_B(G_A(A)) - A|| (Eqn. (2) in the paper)
        Backward cycle loss: lambda_B * ||G_A(G_B(B)) - B|| (Eqn. (2) in the paper)
        Identity loss (optional): lambda_identity * (||G_A(B) - B|| * lambda_B + ||G_B(A) - A|| * lambda_A) (Sec 5.2 "Photo generation from paintings" in the paper)
        Dropout is not used in the original CycleGAN paper.
        """
        # parser.set_defaults(no_dropout=True, no_antialias=True, no_antialias_up=True)  # default CycleGAN did not use dropout
        # parser.set_defaults(no_dropout=True)
        if is_train:
            parser.add_argument('--lambda_A', type=float, default=10.0, help='weight for cycle loss (A -> B -> A)')
            parser.add_argument('--lambda_B', type=float, default=10.0, help='weight for cycle loss (B -> A -> B)')
            parser.add_argument('--lambda_identity', type=float, default=0.5, help='use identity mapping. Setting lambda_identity other than 0 has an effect of scaling the weight of the identity mapping loss. For example, if the weight of the identity loss should be 10 times smaller than the weight of the reconstruction loss, please set lambda_identity = 0.1')

        return parser

    def __init__(self, opt,rank,world_size):
        """Initialize the CycleGAN class.

        Parameters:
            opt (Option class)-- stores all the experiment flags; needs to be a subclass of BaseOptions
        """
        BaseModel.__init__(self, opt)
        # specify the training losses you want to print out. The training/test scripts will call <BaseModel.get_current_losses>
        self.loss_names = ['D_A', 'G_A', 'cycle_A', 'idt_A', 'D_B', 'G_B', 'cycle_B', 'idt_B']
        # specify the images you want to save/display. The training/test scripts will call <BaseModel.get_current_visuals>
        visual_names_A = ['real_A', 'fake_B', 'rec_A']
        visual_names_B = ['real_B', 'fake_A', 'rec_B']
        self.rank=rank
        if self.isTrain and self.opt.lambda_identity > 0.0:  # if identity loss is used, we also visualize idt_B=G_A(B) ad idt_A=G_A(B)
            visual_names_A.append('idt_B')
            visual_names_B.append('idt_A')

        self.visual_names = visual_names_A + visual_names_B  # combine visualizations for A and B
        # specify the models you want to save to the disk. The training/test scripts will call <BaseModel.save_networks> and <BaseModel.load_networks>.
        if self.isTrain:
            self.model_names = ['G_A', 'G_B', 'D_A', 'D_B']
        else:  # during test time, only load Gs
            self.model_names = ['G_A', 'G_B']

        # define networks (both Generators and discriminators)
        # The naming is different from those used in the paper.
        # Code (vs. paper): G_A (G), G_B (F), D_A (D_Y), D_B (D_X)
        self.netG_A = networks.define_G(opt.input_nc, opt.output_nc, opt.output_depth, opt.output_size, opt.ngf, opt.netG, opt.normG, not opt.no_dropout, opt.init_type, opt.init_gain, opt.no_antialias, opt.no_antialias_up, self.gpu_ids, opt,self.rank)
        self.netG_B = networks.define_G(opt.input_nc, opt.output_nc, opt.output_depth, opt.output_size, opt.ngf, opt.netG, opt.normG, not opt.no_dropout, opt.init_type, opt.init_gain, opt.no_antialias, opt.no_antialias_up, self.gpu_ids, opt,self.rank)
        
        if self.isTrain:  # define discriminators
            self.netD_A = networks.define_D(opt.output_nc, opt.ndf, opt.netD, opt.n_layers_D, opt.normD, opt.init_type, opt.init_gain, opt.no_antialias, self.gpu_ids, opt,self.rank)
            self.netD_B = networks.define_D(opt.output_nc, opt.ndf, opt.netD, opt.n_layers_D, opt.normD, opt.init_type, opt.init_gain, opt.no_antialias, self.gpu_ids, opt,self.rank)
            
        if self.isTrain:
            if opt.lambda_identity > 0.0:  # only works when input and output images have the same number of channels
                assert(opt.input_nc == opt.output_nc)
            self.fake_A_pool = ImagePool(opt.pool_size)  # create image buffer to store previously generated images
            self.fake_B_pool = ImagePool(opt.pool_size)  # create image buffer to store previously generated images
            # define loss functions
            self.criterionGAN = networks.GANLoss(opt.gan_mode).to(rank)  # define GAN loss.
            self.criterionCycle = torch.nn.L1Loss().to(rank)
            self.criterionIdt = torch.nn.L1Loss().to(rank)
            # initialize optimizers; schedulers will be automatically created by function <BaseModel.setup>.
            self.optimizer_G = torch.optim.Adam(itertools.chain(self.netG_A.parameters(), self.netG_B.parameters()), lr=opt.lr, betas=(opt.beta1, opt.beta2))
            self.optimizer_D = torch.optim.Adam(itertools.chain(self.netD_A.parameters(), self.netD_B.parameters()), lr=opt.lr, betas=(opt.beta1, opt.beta2))
            self.optimizers.append(self.optimizer_G)
            self.optimizers.append(self.optimizer_D)

    def set_input(self, input):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.

        Parameters:
            input (dict): include the data itself and its metadata information.

        The option 'direction' can be used to swap domain A and domain B.
        """
        AtoB = self.opt.direction == 'AtoB'
        self.real_A = input[0 if AtoB else 1].to(self.rank)#.to(self.device)
        self.real_B = input[1 if AtoB else 0].to(self.rank)#.to(self.device)

    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""
        
        self.fake_B = self.netG_A(self.real_A) #G_A(real_A) - fake B
        
        self.rec_A = self.netG_B(self.fake_B) #G_B(fake_B) - cycle A
        
        self.fake_A = self.netG_B(self.real_B) #G_B(real_B) - fake A
        
        self.rec_B = self.netG_A(self.fake_A) #G_A(fake_A) - cycle B


    def compute_D_loss(self):
        """Calculate GAN loss for the discriminator"""
        fake_B = self.fake_B.detach()
        fake_A = self.fake_A.detach()
        # Fake; stop backprop to the generator by detaching fake_B
        pred_fake_A = self.netD_B(fake_A)
        pred_fake_B = self.netD_A(fake_B)
        self.loss_D_fake_A = self.criterionGAN(pred_fake_A, False).mean()
        self.loss_D_fake_B = self.criterionGAN(pred_fake_B, False).mean()
        self.loss_D_fake = self.loss_D_fake_A + self.loss_D_fake_B 

        # Real
        self.pred_real_A = self.netD_B(self.real_A)
        self.pred_real_B = self.netD_A(self.real_B)
        loss_D_real_A = self.criterionGAN(self.pred_real_A, True)
        loss_D_real_B = self.criterionGAN(self.pred_real_B, True)
        self.loss_D_real_A = loss_D_real_A.mean()
        self.loss_D_real_B = loss_D_real_B.mean()
        self.loss_D_real = self.loss_D_real_A + self.loss_D_real_B 

        # combine loss and calculate gradients
        self.loss_D = (self.loss_D_fake + self.loss_D_real) * 0.5

        return self.loss_D

    def compute_G_loss(self):
        """Calculate GAN and NCE loss for the generator"""
        
        lambda_idt = self.opt.lambda_identity
        lambda_A = self.opt.lambda_A
        lambda_B = self.opt.lambda_B
        
        fake_A = self.fake_A
        fake_B = self.fake_B
        # First, G(A) should fake the discriminator
        pred_fake_A = self.netD_B(fake_A)
        pred_fake_B = self.netD_A(fake_B)
        self.loss_G_A = self.criterionGAN(pred_fake_A, True).mean()
        self.loss_G_B = self.criterionGAN(pred_fake_B, True).mean()
        
        # Identity loss
        if lambda_idt > 0:
            # G_A should be identity if real_B is fed: ||G_A(B) - B||
            self.idt_A = self.netG_A(self.real_B)
            self.loss_idt_A = self.criterionIdt(self.idt_A, self.real_B) * lambda_B * lambda_idt
            # G_B should be identity if real_A is fed: ||G_B(A) - A||
            self.idt_B = self.netG_B(self.real_A)
            self.loss_idt_B = self.criterionIdt(self.idt_B, self.real_A) * lambda_A * lambda_idt
        else:
            self.loss_idt_A = 0
            self.loss_idt_B = 0

        self.loss_cycle_A = self.criterionCycle(self.rec_A, self.real_A) * lambda_A
        # Backward cycle loss || G_A(G_B(B)) - B||
        self.loss_cycle_B = self.criterionCycle(self.rec_B, self.real_B) * lambda_B
        # combined loss and calculate gradients
        self.loss_G = self.loss_G_A + self.loss_G_B + self.loss_cycle_A + self.loss_cycle_B + self.loss_idt_A + self.loss_idt_B
        return self.loss_G

    def data_dependent_initialize(self,data, train=True):
        return

    def get_gen(self):
        return self.netG_A

    def optimize_parameters(self):
        """Calculate losses, gradients, and update network weights; called in every training iteration"""
        # forward
        self.forward()      # compute fake images and reconstruction images.
        # G_A and G_B
        self.set_requires_grad([self.netD_A, self.netD_B], False)  # Ds require no gradients when optimizing Gs
        self.optimizer_G.zero_grad(set_to_none=True)  # set G_A and G_B's gradients to zero
        self.loss_G=self.compute_G_loss()
        self.loss_G.backward()            # calculate gradients for G_A and G_B
        self.optimizer_G.step()       # update G_A and G_B's weights
        # D_A and D_B
        self.set_requires_grad([self.netD_A, self.netD_B], True)
        self.optimizer_D.zero_grad(set_to_none=True)   # set D_A and D_B's gradients to zero
        self.loss_D=self.compute_D_loss()
        self.loss_D.backward()  
        self.optimizer_D.step()  # update D_A and D_B's weights

    def plot_losses_to_tensorboard(self,writer,phase,total_iters):    
        if phase == "train":
            writer.add_scalar('D_real_loss', self.loss_D_real, total_iters)
            writer.add_scalar('D_fake_loss', self.loss_D_fake, total_iters)

            writer.add_scalar('loss_G_CT', self.loss_G_B, total_iters)
            writer.add_scalar('loss_G_CBCT', self.loss_G_A, total_iters)

            writer.add_scalar('identity_loss_CT', self.loss_idt_B, total_iters)
            writer.add_scalar('identity_loss_CBCT', self.loss_idt_A, total_iters)

            writer.add_scalar('cycle_loss_CT', self.loss_cycle_B, total_iters)
            writer.add_scalar('cycle_loss_CBCT', self.loss_cycle_A, total_iters)
        
        writer.add_scalar(f"generator {phase}".format(phase), self.loss_G, total_iters)
        writer.add_scalar(f"discriminator {phase}".format(phase), self.loss_D, total_iters)

    
    def normalize_img(self,img):
        img_norm=(torch.zeros_like(img).cpu()).detach().numpy()
        img_cpu=(img.cpu()).detach().numpy()
        for i in range(img.shape[0]): #batch size
            maxx=np.max(img_cpu[i,:,:,:])
            minn=np.min(img_cpu[i,:,:,:])
            img_norm[i,:,:,:] = (img_cpu[i,:,:,:]-minn)/(maxx-minn) #normalizes the image between 0 and 1 (for tensorboard)
        return torch.tensor(img_norm)

    def plot_images_to_tensorboard(self,writer,phase,total_iters):
        with torch.no_grad():
            random_x=random.randint(0,self.real_A.shape[2]-1)
            
            CBCT_normalized=self.normalize_img(self.real_A)
            CT_normalized=self.normalize_img(self.real_B)
            fake_CT_normalized=self.normalize_img(self.fake_B)
            fake_CBCT_normalized=self.normalize_img(self.fake_A)
        
            img_grid2 = torchvision.utils.make_grid(CBCT_normalized[:,:,random_x,:,:].cpu())
            writer.add_image(f"CBCT_{phase}".format(phase),img_grid2, total_iters)
            writer.close()

            img_grid3 = torchvision.utils.make_grid(fake_CT_normalized[:,:,random_x,:,:].cpu())
            writer.add_image(f"fake_CT_{phase}".format(phase),img_grid3, total_iters)
            writer.close()

            img_grid4 = torchvision.utils.make_grid(CT_normalized[:,:,random_x,:,:].cpu())
            writer.add_image(f"CT_{phase}".format(phase),img_grid4, total_iters)
            writer.close()

            img_grid5 = torchvision.utils.make_grid(fake_CBCT_normalized[:,:,random_x,:,:].cpu())
            writer.add_image(f"fake_CBCT_{phase}".format(phase),img_grid5, total_iters)
            writer.close()

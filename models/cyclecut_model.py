import numpy as np
import torch
from .base_model import BaseModel
from . import networks
#from generator_model_3d import *
from .patchnce import PatchNCELoss
import util.util as util
import itertools
import pdb
import random
import time
import torchvision

class CycleCUTModel(BaseModel):
    """ This class implements CUT and FastCUT model, described in the paper
    Contrastive Learning for Unpaired Image-to-Image Translation
    Taesung Park, Alexei A. Efros, Richard Zhang, Jun-Yan Zhu
    ECCV, 2020

    The code borrows heavily from the PyTorch implementation of CycleGAN
    https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix
    """
    @staticmethod
    def modify_commandline_options(parser, is_train=True):
        """  Configures options specific for CUT model
        """
        parser.add_argument('--CUT_mode', type=str, default="CUT", choices='(CUT, cut, FastCUT, fastcut)')

        parser.add_argument('--lambda_GAN', type=float, default=1.0, help='weight for GAN loss：GAN(G(X))')
        parser.add_argument('--lambda_A', type=float, default=10.0, help='weight for cycle loss (A -> B -> A)')
        parser.add_argument('--lambda_B', type=float, default=10.0, help='weight for cycle loss (B -> A -> B)')
        parser.add_argument('--lambda_identity', type=float, default=0.5, help='use identity mapping. Setting lambda_identity other than 0 has an effect of scaling the weight of the identity mapping loss. For example, if the weight of the identity loss should be 10 times smaller than the weight of the reconstruction loss, please set lambda_identity = 0.1')
        parser.add_argument('--lambda_NCE', type=float, default=1.0, help='weight for NCE loss: NCE(G(X), X)')
        parser.add_argument('--nce_idt', type=util.str2bool, nargs='?', const=True, default=False, help='use NCE loss for identity mapping: NCE(G(Y), Y))')
        parser.add_argument('--nce_layers', type=str, default='0,4,8,12,16', help='compute NCE loss on which layers')
        parser.add_argument('--nce_includes_all_negatives_from_minibatch',
                            type=util.str2bool, nargs='?', const=True, default=False,
                            help='(used for single image translation) If True, include the negatives from the other samples of the minibatch when computing the contrastive loss. Please see models/patchnce.py for more details.')
        parser.add_argument('--netF', type=str, default='mlp_sample', choices=['sample', 'reshape', 'mlp_sample'], help='how to downsample the feature map')
        parser.add_argument('--netF_nc', type=int, default=256)
        parser.add_argument('--nce_T', type=float, default=0.07, help='temperature for NCE loss')
        parser.add_argument('--num_patches', type=int, default=256, help='number of patches per layer')
        parser.add_argument('--flip_equivariance',
                            type=util.str2bool, nargs='?', const=True, default=False,
                            help="Enforce flip-equivariance as additional regularization. It's used by FastCUT, but not CUT")

        parser.set_defaults(pool_size=0)  # no image pooling

        opt, _ = parser.parse_known_args()

        # Set default parameters for CUT and FastCUT
        if opt.CUT_mode.lower() == "cut":
            parser.set_defaults(nce_idt=True, lambda_NCE=1.0)
        elif opt.CUT_mode.lower() == "fastcut":
            parser.set_defaults(
                nce_idt=False, lambda_NCE=10.0, flip_equivariance=True,
                n_epochs=150, n_epochs_decay=50
            )
        else:
            raise ValueError(opt.CUT_mode)

        return parser

    def __init__(self, opt, rank, world_size):
        BaseModel.__init__(self, opt)

        # specify the training losses you want to print out.
        # The training/test scripts will call <BaseModel.get_current_losses>
        self.loss_names = ['D_A', 'G_A', 'cycle_A', 'idt_A', 'NCE_A', 'D_B', 'G_B', 'cycle_B', 'idt_B', 'NCE_B']#['G_GAN', 'D_real', 'D_fake', 'G', 'NCE']
        self.visual_names_A = ['real_A', 'fake_A', 'rec_A']
        self.visual_names_B = ['real_B', 'fake_B', 'rec_B']
        self.nce_layers = [int(i) for i in self.opt.nce_layers.split(',')]
        self.rank=rank
        if opt.nce_idt and self.isTrain:
            self.loss_names += ['NCE_Y_A','NCE_Y_B']
        
        if self.isTrain and self.opt.lambda_identity > 0.0:  # if identity loss is used, we also visualize idt_B=G_A(B) ad idt_A=G_A(B)
            self.visual_names_A.append('idt_B')
            self.visual_names_B.append('idt_A')

        self.visual_names = self.visual_names_A + self.visual_names_B  # combine visualizations for A and B

        # specify the models you want to save to the disk. The training/test scripts will call <BaseModel.save_networks> and <BaseModel.load_networks>.
        if self.isTrain:
            self.model_names = ['G_A', 'G_B', 'D_A', 'D_B', 'F_A', 'F_B']
        else:  # during test time, only load Gs
            self.model_names = ['G_A']

        # define networks (both generator and discriminator) #
        self.netG_A = networks.define_G(opt.input_nc, opt.output_nc, opt.output_depth, opt.output_size, opt.ngf, opt.netG, opt.normG, not opt.no_dropout, opt.init_type, opt.init_gain, opt.no_antialias, opt.no_antialias_up, self.gpu_ids, opt,self.rank)
        self.netG_B = networks.define_G(opt.input_nc, opt.output_nc, opt.output_depth, opt.output_size, opt.ngf, opt.netG, opt.normG, not opt.no_dropout, opt.init_type, opt.init_gain, opt.no_antialias, opt.no_antialias_up, self.gpu_ids, opt,self.rank)
           
        self.netF_A = networks.define_F(opt.input_nc, opt.netF, opt.normG, not opt.no_dropout, opt.init_type, opt.init_gain, opt.no_antialias, self.gpu_ids, opt,self.rank)
        self.netF_B = networks.define_F(opt.input_nc, opt.netF, opt.normG, not opt.no_dropout, opt.init_type, opt.init_gain, opt.no_antialias, self.gpu_ids, opt,self.rank)
        
        if self.isTrain:  # define discriminators
            self.netD_A = networks.define_D(opt.output_nc, opt.ndf, opt.netD, opt.n_layers_D, opt.normD, opt.init_type, opt.init_gain, opt.no_antialias, self.gpu_ids, opt,self.rank)
            self.netD_B = networks.define_D(opt.output_nc, opt.ndf, opt.netD, opt.n_layers_D, opt.normD, opt.init_type, opt.init_gain, opt.no_antialias, self.gpu_ids, opt,self.rank)

        if self.isTrain:
            # define loss functions
            self.criterionNCE = []
            for nce_layer in self.nce_layers:
                self.criterionNCE.append(PatchNCELoss(opt).to(self.device))
            
            self.criterionGAN = networks.GANLoss(opt.gan_mode).to(rank)  # define GAN loss.
            self.criterionCycle = torch.nn.L1Loss().to(rank)
            self.criterionIdt = torch.nn.L1Loss().to(rank)

            # initialize optimizers; schedulers will be automatically created by function <BaseModel.setup>.
            self.optimizer_G = torch.optim.Adam(itertools.chain(self.netG_A.parameters(), self.netG_B.parameters()), lr=opt.lr, betas=(opt.beta1, opt.beta2))
            self.optimizer_D = torch.optim.Adam(itertools.chain(self.netD_A.parameters(), self.netD_B.parameters()), lr=opt.lr, betas=(opt.beta1, opt.beta2))
            self.optimizers.append(self.optimizer_G)
            self.optimizers.append(self.optimizer_D)

    def data_dependent_initialize(self, data, train=True):
        """
        The feature network netF is defined in terms of the shape of the intermediate, extracted
        features of the encoder portion of netG. Because of this, the weights of netF are
        initialized at the first feedforward pass with some input images.
        Please also see PatchSampleF.create_mlp(), which is called at the first forward() call.
        """
        bs_per_gpu = data[0].size(0) // max(len(self.opt.gpu_ids), 1)
        self.set_input(data)
        self.real_A = self.real_A[:bs_per_gpu]
        self.real_B = self.real_B[:bs_per_gpu]
        self.forward()                     # compute fake images: G(A)
        if train:
            self.compute_D_loss().backward()                   # calculate gradients for D
            self.compute_G_loss().backward()                   # calculate graidents for G
            if self.opt.lambda_NCE > 0.0:
                self.optimizer_F = torch.optim.Adam(itertools.chain(self.netF_A.parameters(), self.netF_B.parameters()), lr=self.opt.lr, betas=(self.opt.beta1, self.opt.beta2))
                self.optimizers.append(self.optimizer_F)

    def get_gen(self):
        return self.netG_A

    def set_input(self, input):
        """Unpack input data from the dataloader and perform necessary pre-processing steps.

        Parameters:
            input (dict): include the data itself and its metadata information.

        The option 'direction' can be used to swap domain A and domain B.
        """
        AtoB = self.opt.direction == 'AtoB'
        self.real_A = input[0 if AtoB else 1].to(self.rank)#.to(self.device)
        self.real_B = input[1 if AtoB else 0].to(self.rank)#.to(self.device)

    def forward(self):
        """Run forward pass; called by both functions <optimize_parameters> and <test>."""
        
        self.fake_B = self.netG_A(self.real_A) #G_A(real_A) - fake B
        
        self.rec_A = self.netG_B(self.fake_B) #G_B(fake_B) - cycle A
        
        self.fake_A = self.netG_B(self.real_B) #G_B(real_B) - fake A
        
        self.rec_B = self.netG_A(self.fake_A) #G_A(fake_A) - cycle B


    def compute_D_loss(self):
        """Calculate GAN loss for the discriminator"""
        fake_B = self.fake_B.detach()
        fake_A = self.fake_A.detach()
        # Fake; stop backprop to the generator by detaching fake_B
        pred_fake_A = self.netD_B(fake_A)
        pred_fake_B = self.netD_A(fake_B)
        self.loss_D_fake_A = self.criterionGAN(pred_fake_A, False).mean()
        self.loss_D_fake_B = self.criterionGAN(pred_fake_B, False).mean()
        self.loss_D_fake = self.loss_D_fake_A + self.loss_D_fake_B 

        # Real
        self.pred_real_A = self.netD_B(self.real_A)
        self.pred_real_B = self.netD_A(self.real_B)
        loss_D_real_A = self.criterionGAN(self.pred_real_A, True)
        loss_D_real_B = self.criterionGAN(self.pred_real_B, True)
        self.loss_D_real_A = loss_D_real_A.mean()
        self.loss_D_real_B = loss_D_real_B.mean()
        self.loss_D_real = self.loss_D_real_A + self.loss_D_real_B 

        # combine loss and calculate gradients
        self.loss_D = (self.loss_D_fake + self.loss_D_real) * 0.5

        return self.loss_D

    def compute_G_loss(self):
        """Calculate GAN and NCE loss for the generator"""
        
        lambda_idt = self.opt.lambda_identity
        lambda_A = self.opt.lambda_A
        lambda_B = self.opt.lambda_B
        
        fake_A = self.fake_A
        fake_B = self.fake_B
        # First, G(A) should fake the discriminator
        if self.opt.lambda_GAN > 0.0:
            pred_fake_A = self.netD_B(fake_A)
            pred_fake_B = self.netD_A(fake_B)
            self.loss_G_A = self.criterionGAN(pred_fake_A, True).mean() * self.opt.lambda_GAN
            self.loss_G_B = self.criterionGAN(pred_fake_B, True).mean() * self.opt.lambda_GAN
        else:
            self.loss_G_A = 0.0
            self.loss_G_B = 0.0
        
        # Identity loss
        if lambda_idt > 0:
            # G_A should be identity if real_B is fed: ||G_A(B) - B||
            self.idt_A = self.netG_A(self.real_B)
            self.loss_idt_A = self.criterionIdt(self.idt_A, self.real_B) * lambda_B * lambda_idt
            # G_B should be identity if real_A is fed: ||G_B(A) - A||
            self.idt_B = self.netG_B(self.real_A)
            self.loss_idt_B = self.criterionIdt(self.idt_B, self.real_A) * lambda_A * lambda_idt
        else:
            self.loss_idt_A = 0
            self.loss_idt_B = 0

        self.loss_cycle_A = self.criterionCycle(self.rec_A, self.real_A) * lambda_A
        # Backward cycle loss || G_A(G_B(B)) - B||
        self.loss_cycle_B = self.criterionCycle(self.rec_B, self.real_B) * lambda_B
        # combined loss and calculate gradients
        
        
        if self.opt.lambda_NCE > 0.0:
            self.loss_NCE_A = self.calculate_NCE_loss_A(self.real_A, self.fake_B)
            self.loss_NCE_Y_A = self.calculate_NCE_loss_A(self.fake_B, self.idt_A)
            self.loss_NCE_A_tot=(self.loss_NCE_A+self.loss_NCE_Y_A)*0.5
    
            self.loss_NCE_B = self.calculate_NCE_loss_B(self.real_B, self.fake_A)
            self.loss_NCE_Y_B = self.calculate_NCE_loss_B(self.fake_A, self.idt_B)
            self.loss_NCE_B_tot=(self.loss_NCE_B+self.loss_NCE_Y_B)*0.5
        else:
            self.loss_NCE_A_tot, self.loss_NCE_B_tot, self.loss_NCE_bd = 0.0, 0.0, 0.0
        
        self.loss_G = self.loss_G_A + self.loss_G_B + self.loss_cycle_A + self.loss_cycle_B + self.loss_idt_A + self.loss_idt_B+(self.loss_NCE_A_tot+self.loss_NCE_B_tot)*0.1
        return self.loss_G

    def optimize_parameters(self):
        """Calculate losses, gradients, and update network weights; called in every training iteration"""
        # forward
        self.forward()      # compute fake images and reconstruction images.
        # G_A and G_B
        self.set_requires_grad([self.netD_A, self.netD_B], False)  # Ds require no gradients when optimizing Gs
        self.optimizer_G.zero_grad(set_to_none=True)  # set G_A and G_B's gradients to zero
        if self.opt.netF == 'mlp_sample':
            self.optimizer_F.zero_grad(set_to_none=True)
        self.loss_G=self.compute_G_loss()
        self.loss_G.backward()            # calculate gradients for G_A and G_B
        self.optimizer_G.step()       # update G_A and G_B's weights
        if self.opt.netF == 'mlp_sample':
            self.optimizer_F.step()
        # D_A and D_B
        self.set_requires_grad([self.netD_A, self.netD_B], True)
        self.optimizer_D.zero_grad(set_to_none=True)   # set D_A and D_B's gradients to zero
        self.loss_D=self.compute_D_loss()
        self.loss_D.backward()  
        self.optimizer_D.step()  # update D_A and D_B's weights

    def calculate_NCE_loss_A(self, src, tgt):
        n_layers = len(self.nce_layers)
        #pdb.set_trace()
        feat_q = self.netG_A(tgt, self.nce_layers, encode_only=True)

        feat_k = self.netG_A(src, self.nce_layers, encode_only=True)
        feat_k_pool, sample_ids = self.netF_A(feat_k, self.opt.num_patches, None)
        feat_q_pool, _ = self.netF_A(feat_q, self.opt.num_patches, sample_ids)

        total_nce_loss = 0.0
        for f_q, f_k, crit, nce_layer in zip(feat_q_pool, feat_k_pool, self.criterionNCE, self.nce_layers):
            loss = crit(f_q, f_k) * self.opt.lambda_NCE
            total_nce_loss += loss.mean()
        
        return total_nce_loss / n_layers

    def calculate_NCE_loss_B(self, src, tgt):
        n_layers = len(self.nce_layers)
        #pdb.set_trace()
        feat_q = self.netG_B(tgt, self.nce_layers, encode_only=True)

        feat_k = self.netG_B(src, self.nce_layers, encode_only=True)
        feat_k_pool, sample_ids = self.netF_B(feat_k, self.opt.num_patches, None)
        feat_q_pool, _ = self.netF_B(feat_q, self.opt.num_patches, sample_ids)

        total_nce_loss = 0.0
        for f_q, f_k, crit, nce_layer in zip(feat_q_pool, feat_k_pool, self.criterionNCE, self.nce_layers):
            loss = crit(f_q, f_k) * self.opt.lambda_NCE
            total_nce_loss += loss.mean()
        
        return total_nce_loss / n_layers

    def plot_losses_to_tensorboard(self,writer,phase,total_iters):    
        if phase == "train":
            writer.add_scalar('D_real_loss', self.loss_D_real, total_iters)
            writer.add_scalar('D_fake_loss', self.loss_D_fake, total_iters)

            writer.add_scalar('identity_loss_CT', self.loss_idt_B, total_iters)
            writer.add_scalar('identity_loss_CBCT', self.loss_idt_A, total_iters)

            writer.add_scalar('cycle_loss_CT', self.loss_cycle_B, total_iters)
            writer.add_scalar('cycle_loss_CBCT', self.loss_cycle_A, total_iters)
        
            writer.add_scalar('Loss_NCE_A', self.loss_NCE_A_tot, total_iters)
            writer.add_scalar('Loss_NCE_B', self.loss_NCE_B_tot, total_iters)
            
            writer.add_scalar('loss_G_CT', self.loss_G_B, total_iters)
            writer.add_scalar('loss_G_CBCT', self.loss_G_A, total_iters)

        writer.add_scalar(f"generator {phase}".format(phase), self.loss_G, total_iters)
        writer.add_scalar(f"discriminator {phase}".format(phase), self.loss_D, total_iters)

    
    def normalize_img(self,img):
        img_norm=(torch.zeros_like(img).cpu()).detach().numpy()
        img_cpu=(img.cpu()).detach().numpy()
        for i in range(img.shape[0]): #batch size
            maxx=np.max(img_cpu[i,:,:,:])
            minn=np.min(img_cpu[i,:,:,:])
            img_norm[i,:,:,:] = (img_cpu[i,:,:,:]-minn)/(maxx-minn) #normalizes the image between 0 and 1 (for tensorboard)
        return torch.tensor(img_norm)

    def plot_images_to_tensorboard(self,writer,phase,total_iters):
        with torch.no_grad():
            random_x=random.randint(0,self.real_A.shape[2]-1)
            
            CBCT_normalized=self.normalize_img(self.real_A)
            CT_normalized=self.normalize_img(self.real_B)
            fake_CT_normalized=self.normalize_img(self.fake_B)
            fake_CBCT_normalized=self.normalize_img(self.fake_A)
        
            img_grid2 = torchvision.utils.make_grid(CBCT_normalized[:,:,random_x,:,:].cpu())
            writer.add_image(f"CBCT_{phase}".format(phase),img_grid2, total_iters)
            writer.close()

            img_grid3 = torchvision.utils.make_grid(fake_CT_normalized[:,:,random_x,:,:].cpu())
            writer.add_image(f"fake_CT_{phase}".format(phase),img_grid3, total_iters)
            writer.close()

            img_grid4 = torchvision.utils.make_grid(CT_normalized[:,:,random_x,:,:].cpu())
            writer.add_image(f"CT_{phase}".format(phase),img_grid4, total_iters)
            writer.close()

            img_grid5 = torchvision.utils.make_grid(fake_CBCT_normalized[:,:,random_x,:,:].cpu())
            writer.add_image(f"fake_CBCT_{phase}".format(phase),img_grid5, total_iters)
            writer.close()

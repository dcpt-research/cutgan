import os
import pydicom
import dicom2nifti
import SimpleITK as sitk
from numpy import mean
import numpy as np
import tqdm
import nibabel
from options.train_options import TrainOptions
from models import *#create_model
import os
import torch.multiprocessing as mp
import torch.distributed as dist
import torch
import pdb
import random

def reverse_normalization(img):
    maxx=3071
    minn=-1000
    img_reversed_norm = (img-(-1))*(maxx-minn)/2+minn
    return img_reversed_norm

def normalize_img(img):
    maxx=3071#np.max(img)
    minn=-1000#np.min(img)
    img_norm = 2*(img-minn)/(maxx-minn)-1 #normalizes the image between -1 and 1 
    return torch.tensor(img_norm)

def img_threshold(img, max, min):
    img[img > max] = max
    img[img < min] = min
    return img

def convertNsave(arr,file_dir,CT_slice,random_UID):
    """
    `arr`: parameter will take a numpy array that represents only one slice.
    `file_dir`: parameter will take the path to save the slices
    `index`: parameter will represent the index of the slice, so this parameter will be used to put 
    the name of each slice while using a for loop to convert all the slices
    """
    fake_CT_img = arr.cpu().detach().numpy()
    
    arr=reverse_normalization(fake_CT_img)

    dicom_file = pydicom.dcmread(CT_slice)
    b = dicom_file.RescaleIntercept
    m = dicom_file.RescaleSlope
    arr = (arr-b)/m
    #print(np.max(arr))
    #Arr=a
    dicom_file.Rows = arr.shape[0]
    dicom_file.Columns = arr.shape[1]
    dicom_file.PhotometricInterpretation = "MONOCHROME2"
    dicom_file.SamplesPerPixel = 1
    dicom_file.BitsStored = 16
    dicom_file.BitsAllocated = 16
    dicom_file.HighBit = 15
    dicom_file.PixelRepresentation = 1            
    dicom_file.PixelData = arr.astype(np.uint16).tobytes() #MR header with fake CT image
         
        #modify header
    dicom_file.SOPInstanceUID = dicom_file.SOPInstanceUID[0:30] + str(random_UID) + dicom_file.SOPInstanceUID[-15:]
    #dicom_file.SeriesInstanceUID = dicom_file.SOPInstanceUID[0:54] + str(np.random.randint(00000,99999))
    dicom_file.StudyDescription += "_fake-CT"
    #dicom_file.SeriesDescription += "_fake-CT"

    #dicom_file.PixelData = arr.tobytes()
    dicom_file.save_as(os.path.join(file_dir))

def nifti2dicom_1file(arr,nifti_dir, out_dir, original_img_path):


    """
    This function is to convert only one nifti file into dicom series
    `nifti_dir`: the path to the one nifti file
    `out_dir`: the path to output
    """
    number_slices = arr.shape[0]
    random_UID=random.randint(00000,99999)
    #for slice_ in range(0,number_slices):
    for slice_ in range(4,104):
        #dcm_img="{}.dcm".format(slice_+1)
        dcm_img="{}.dcm".format(slice_+1)
        slice_=slice_-4
        dcm_path=os.path.join(original_img_path,dcm_img)
        #nifti_slice=nifti_array[:,:,slice_]
        #nifti_slice=np.rot90(nifti_slice, k=1, axes=(0, 1))
        
        out_path=os.path.join(out_dir,dcm_img)
        convertNsave(arr[slice_,:,:],out_path,dcm_path,random_UID)

def dcm_to_nifti(patient_id, input_folder, file, root_nifti):
    
    nii_out = os.path.join(root_nifti, patient_id)
    if not os.path.exists(nii_out):
            os.makedirs(nii_out)

    #for folder in os.listdir(input_folder): #each folder in processed_data_CBCT is a specific CBCT scan of a specific patient
    dicom2nifti.dicom_series_to_nifti(os.path.join(input_folder),os.path.join(nii_out,"{}.nii.gz".format(file)),reorient_nifti=False)
   
def prep_data(data):
    image=sitk.ReadImage(data, sitk.sitkFloat32)
    arr=sitk.GetArrayFromImage(image)
    img_thresh=img_threshold(arr,max=3071,min=-1000)
    normalized_img=normalize_img(img_thresh)
    return normalized_img

def setup(rank, world_size):
    os.environ['MASTER_ADDR'] = 'localhost'
    os.environ['MASTER_PORT'] = '12355'
    dist.init_process_group("nccl", rank=rank, world_size=world_size)

def cleanup():
    dist.destroy_process_group()

def main(rank, world_size):    
    """set:
    --name 'Cut_model' (or 'CycleGAN_model' or 'CycleCUT_model')
    --model 'cut' (or 'cycle_gan' or 'cyclecut')
    --output_size 512
    --output_depth 106
    --checkpoints_dir './my_checkpoints' #remember to more the checkpoints to my_checkpoints
    --continue_train
    --epoch 'latest' (or '100', '150' etc) #what checkpoint to load

    CUDA_VISIBLE_DEVICES=1,3 python test.py --model 'cut' --name 'Cut_model' --checkpoints_dir './my_checkpoints' --output_size 512 --output_depth 106 --epoch 'latest' --continue_train
    
    """

    ### REMEMBER TO SET self.IsTrain=False in train_options

    setup(rank, world_size)
    opt = TrainOptions().parse()
    model = create_model(opt,rank,world_size)
    if rank==0:
        model.setup(opt)
        gen = model.get_gen()
        print("Generating synthic CT using the", opt.model, "model")
        root_in='/data/caspdv/test2' #root to test images
        root_nifti='/data/caspdv/data_nifti_test' #root to save nifti
        root_out='/data/caspdv/data_out' #root to test images
        patients=os.listdir(root_in) #gives us a list of the 20 patients
        for patient in patients: #loop through the patients
            in_path_patient=os.path.join(root_in,patient) #get total path to patients
            files_in_patient=os.listdir(in_path_patient) #list of scans - should just be 1
            for file in files_in_patient:
                in_path = os.path.join(in_path_patient, file) #add file to the path
                print("Turning dicom to nifti for", patient)
                dcm_to_nifti(patient,in_path,file,root_nifti) #makes a nifti file from the CBCT
                print("Done")
            nii_out = os.path.join(root_nifti, patient)
            nifti_files = os.listdir(nii_out)
            for file in nifti_files:
                file_no_suffix=file.removesuffix(".nii.gz")
                model_specfic_file=opt.model+file_no_suffix
                in_path = os.path.join(nii_out, file)
                
                print("Prepering image for synthetic CT generation")
                image=prep_data(in_path)
                image=torch.unsqueeze(image,dim=0)
                print("Done")
                
                with torch.no_grad():
                    print("Synthetic CT generation.....")
                    sCT=gen(image) #make function in model to take generator and make it work on image
                    print("Done")
                sCT=torch.squeeze(sCT)
                out_path = os.path.join(root_out, patient, model_specfic_file) #gives the id without the suffix for saving
                if not os.path.exists(out_path):
                    os.makedirs(out_path)
                print("Saving Synthetic CT....")
                original_CBCT_img_path=os.path.join(root_in,patient,file_no_suffix) #gives us the original CBCT where the
                nifti2dicom_1file(sCT,in_path, out_path, original_CBCT_img_path)
        print("Synthetic CTs generated and saved")
    cleanup()


if __name__ == '__main__':
    # suppose we have 3 gpus
    world_size = 2
    mp.spawn(
        main,
        args=(world_size,),
        nprocs=world_size
    )
    